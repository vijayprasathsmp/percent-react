import { useEffect, useState } from "react";
import Card from "../../card";
import Progress from "../../progress";
import Dialog from "@mui/material/Dialog";
import AddCircleIcon from "@mui/icons-material/AddCircle";
import axios from "axios";

const DashBoard = () => {
  const [isRefresh, setREfresh] = useState(false);
  const [isOpen, setIsOpen] = useState(false);
  const [isSkill, setSkill] = useState(false);
  const [isEdit, setEdit] = useState(false);
  const [dashboardData, setDashboardData] = useState([]);
  const [dashboardData1, setDashboardData1] = useState([]);
  const [skillData, setSkillData] = useState([]);
  const [listStudent, setStudentList] = useState([]);
  const [editData, setEditData] = useState({});
  const instance = axios.create({
    baseURL: "http://localhost:3002/api",
    headers: {
      Authorization: "Bearer " + localStorage.getItem("token"),
    },
  });

  useEffect(() => {
    // list dashboard
    instance
      .get("/skill/dashboard")
      .then((data) => {
        console.log(data.status);
        if (data?.data?.data) {
          setDashboardData(data?.data?.data);
        } else {
          setDashboardData([]);
        }
      })
      .catch((err) => {
        window.location.pathname = "/login";

        console.log("dashboard list err", err);
      });
    // second calculation
    instance
      .get("/skill/dashboard1")
      .then((data) => {
        console.log(data.status);
        if (data?.data?.data) {
          setDashboardData1(data?.data?.data);
        } else {
          setDashboardData1([]);
        }
      })
      .catch((err) => {
        window.location.pathname = "/login";

        console.log("dashboard list err", err);
      });
    // list students
    instance
      .get("/student", {})
      .then((data) => {
        if (data?.data?.data) {
          setStudentList(data?.data?.data);
        } else {
          setStudentList([]);
        }
      })
      .catch((err) => {
        console.log("student list err", err);
      });
    // list skills
    instance
      .get("/skill", {})
      .then((data) => {
        if (data?.data?.data) {
          setSkillData(data?.data?.data);
        } else {
          setSkillData([]);
        }
      })
      .catch((err) => {
        console.log("skill list err", err);
      });
  }, [isRefresh]);

  return (
    <>
      {/* dialog */}
      <Dialog
        open={isOpen}
        onClose={() => {
          setIsOpen(false);
        }}
      >
        <div className="p-5 max-h-[500px] min-w-[500px] overflow-x-auto">
          <div>
            <p className="text-xl mb-5 font-bold text-navy-700 ">
              {isSkill
                ? isEdit
                  ? "Edit"
                  : "Add" + " Skill"
                : isEdit
                ? "Edit"
                : "Add" + " Student"}
            </p>
          </div>
          {!isSkill ? (
            <PopUp
              instance={instance}
              editData={editData}
              closeOpen={() => {
                setIsOpen(false);
                setEdit(false);
                setREfresh(!isRefresh);
              }}
              skillData={skillData}
              isEdit={isEdit}
            />
          ) : (
            <SkillPopUp
              instance={instance}
              closeOpen={() => {
                setIsOpen(false);
                setEdit(false);
                setREfresh(!isRefresh);
              }}
            />
          )}
        </div>
      </Dialog>

      <div className="my-4 flex justify-between items-center">
        <div class="text-xl font-bold text-navy-700 ">Dashboard</div>
        <div>
          <svg
            onClick={() => {
              localStorage.removeItem("token");
              window.location.pathname = "/login";
            }}
            className="cursor-pointer"
            width="30"
            height="30px"
            viewBox="0 0 24 24"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              d="M22 6.62219V17.245C22 18.3579 21.2857 19.4708 20.1633 19.8754L15.0612 21.7977C14.7551 21.8988 14.449 22 14.0408 22C13.5306 22 12.9184 21.7977 12.4082 21.4942C12.2041 21.2918 11.898 21.0895 11.7959 20.8871H7.91837C6.38776 20.8871 5.06122 19.6731 5.06122 18.0544V17.0427C5.06122 16.638 5.36735 16.2333 5.87755 16.2333C6.38776 16.2333 6.69388 16.5368 6.69388 17.0427V18.0544C6.69388 18.7626 7.30612 19.2684 7.91837 19.2684H11.2857V4.69997H7.91837C7.20408 4.69997 6.69388 5.20582 6.69388 5.91401V6.9257C6.69388 7.33038 6.38776 7.73506 5.87755 7.73506C5.36735 7.73506 5.06122 7.33038 5.06122 6.9257V5.91401C5.06122 4.39646 6.28572 3.08125 7.91837 3.08125H11.7959C12 2.87891 12.2041 2.67657 12.4082 2.47423C13.2245 1.96838 14.1429 1.86721 15.0612 2.17072L20.1633 4.09295C21.1837 4.39646 22 5.50933 22 6.62219Z"
              fill="#4318ff"
            />
            <path
              d="M4.85714 14.8169C4.65306 14.8169 4.44898 14.7158 4.34694 14.6146L2.30612 12.5912C2.20408 12.49 2.20408 12.3889 2.10204 12.3889C2.10204 12.2877 2 12.1865 2 12.0854C2 11.9842 2 11.883 2.10204 11.7819C2.10204 11.6807 2.20408 11.5795 2.30612 11.5795L4.34694 9.55612C4.65306 9.25261 5.16327 9.25261 5.46939 9.55612C5.77551 9.85963 5.77551 10.3655 5.46939 10.669L4.7551 11.3772H8.93878C9.34694 11.3772 9.7551 11.6807 9.7551 12.1865C9.7551 12.6924 9.34694 12.7936 8.93878 12.7936H4.65306L5.36735 13.5017C5.67347 13.8052 5.67347 14.3111 5.36735 14.6146C5.26531 14.7158 5.06122 14.8169 4.85714 14.8169Z"
              fill="#4318ff"
            />
          </svg>
        </div>
      </div>

      {/* content */}
      <div className="flex">
        <Card extra={"w-[500px] h-full px-6 pb-6 sm:overflow-x-auto"}>
          <div class="relative flex items-center justify-between pt-4">
            <div class="text-sm font-bold text-navy-700 ">
              {" "}
              total available skill bashed showing Percentage
            </div>
          </div>
          {/* table content */}
          <div class="mt-8 overflow-x-scroll xl:overflow-hidden">
            <table className="w-full">
              <thead>
                <tr>
                  <th className="border-b border-gray-200 pr-28 pb-[10px] text-start ">
                    <p className="text-xs tracking-wide text-gray-600">
                      Skills
                    </p>
                  </th>
                  <th className="border-b border-gray-200 pr-28 pb-[10px] text-start ">
                    <p className="text-xs tracking-wide text-gray-600">
                      Percentage
                    </p>
                  </th>
                  <th className="border-b border-gray-200 pr-28 pb-[10px] text-start ">
                    <p className="text-xs tracking-wide text-gray-600">
                      Progress
                    </p>
                  </th>
                </tr>
              </thead>

              <tbody>
                {dashboardData.length == 0 ? (
                  <p className="text-sm font-bold text-navy-700 ">
                    No Skill was Found Please add skills after you create
                    student
                  </p>
                ) : (
                  dashboardData.map((e, index) => {
                    return (
                      <tr key={index}>
                        <td className="pt-[14px] pb-[18px] sm:text-[14px]">
                          <p className="text-sm font-bold text-navy-700 ">
                            {e.name}
                          </p>
                        </td>
                        <td className="pt-[14px] pb-[18px] sm:text-[14px]">
                          <p className="text-sm font-bold text-navy-700 ">
                            {e.percentage.toFixed(2)}%
                          </p>
                        </td>
                        <td className="pt-[14px] pb-[18px] sm:text-[14px]">
                          <Progress
                            width="w-[108px]"
                            color="indigo"
                            value={e.percentage.toFixed(2)}
                          />
                        </td>
                      </tr>
                    );
                  })
                )}
              </tbody>
            </table>
          </div>
        </Card>
        {/* second */}
        <Card extra={"w-[500px] ml-5 h-full px-6 pb-6 sm:overflow-x-auto"}>
          <div class="relative flex items-center justify-between pt-4">
            <div class="text-sm font-bold text-navy-700 ">
              {" "}
              total available student bashed showing Percentage
            </div>
          </div>
          {/* table content */}
          <div class="mt-8 overflow-x-scroll xl:overflow-hidden">
            <table className="w-full">
              <thead>
                <tr>
                  <th className="border-b border-gray-200 pr-28 pb-[10px] text-start ">
                    <p className="text-xs tracking-wide text-gray-600">
                      Skills
                    </p>
                  </th>
                  <th className="border-b border-gray-200 pr-28 pb-[10px] text-start ">
                    <p className="text-xs tracking-wide text-gray-600">
                      Percentage
                    </p>
                  </th>
                  <th className="border-b border-gray-200 pr-28 pb-[10px] text-start ">
                    <p className="text-xs tracking-wide text-gray-600">
                      Progress
                    </p>
                  </th>
                </tr>
              </thead>

              <tbody>
                {dashboardData1.length == 0 ? (
                  <p className="text-sm font-bold text-navy-700 ">
                    No Skill was Found Please add skills after you create
                    student
                  </p>
                ) : (
                  dashboardData1.map((e, index) => {
                    return (
                      <tr key={index}>
                        <td className="pt-[14px] pb-[18px] sm:text-[14px]">
                          <p className="text-sm font-bold text-navy-700 ">
                            {e.name}
                          </p>
                        </td>
                        <td className="pt-[14px] pb-[18px] sm:text-[14px]">
                          <p className="text-sm font-bold text-navy-700 ">
                            {e.percentage.toFixed(2)}%
                          </p>
                        </td>
                        <td className="pt-[14px] pb-[18px] sm:text-[14px]">
                          <Progress
                            width="w-[108px]"
                            color="indigo"
                            value={e.percentage.toFixed(2)}
                          />
                        </td>
                      </tr>
                    );
                  })
                )}
              </tbody>
            </table>
          </div>
        </Card>
      </div>

      {/* list students */}
      <Card extra={" h-full px-6 pb-6 sm:overflow-x-auto mt-[40px]"}>
        <div class="relative flex items-center justify-between pt-4">
          <div class="text-xl font-bold text-navy-700 ">Student List</div>
          {/* actions */}
          <div className="flex">
            <div
              onClick={() => {
                if (dashboardData.length != 0) {
                  setIsOpen(true);
                  setEdit(false);
                  setSkill(false);
                } else {
                  alert("first add skills after create students");
                }
              }}
              className="bg-[blue] cursor-pointer rounded-[15px] text-xs tracking-wide text-white px-3 py-2"
            >
              Add Student
            </div>
            <div
              onClick={() => {
                setIsOpen(true);
                setEdit(false);
                setSkill(true);
              }}
              className="bg-[blue] ml-4 cursor-pointer rounded-[15px] text-xs tracking-wide text-white px-3 py-2"
            >
              Add Skill
            </div>
          </div>
        </div>
        {/* table content */}
        <div class="mt-8 overflow-x-scroll xl:overflow-hidden">
          <table className="w-full">
            <thead>
              <tr>
                <th className="border-b border-gray-200 pr-28 pb-[10px] text-start ">
                  <p className="text-xs tracking-wide text-gray-600">
                    Student Name
                  </p>
                </th>
                <th className="border-b border-gray-200 pr-28 pb-[10px] text-start ">
                  <p className="text-xs tracking-wide text-gray-600">
                    Student Role
                  </p>
                </th>
                <th className="border-b border-gray-200 pr-28 pb-[10px] text-start ">
                  <p className="text-xs tracking-wide text-gray-600">
                    Skill Name
                  </p>
                </th>
                <th className="border-b border-gray-200 pr-28 pb-[10px] text-start ">
                  <p className="text-xs tracking-wide text-gray-600">
                    Skill Percentage
                  </p>
                </th>
                <th className="border-b border-gray-200 pr-28 pb-[10px] text-start ">
                  <p className="text-xs tracking-wide text-gray-600">
                    Total Skill Percentage
                  </p>
                </th>
                <th className="border-b border-gray-200 pr-28 pb-[10px] text-start ">
                  <p className="text-xs tracking-wide text-gray-600">Action</p>
                </th>
              </tr>
            </thead>
            <tbody>
              {listStudent?.map((e, index) => {
                return (
                  <>
                    <tr key={index}>
                      <td className="pt-[14px] pb-[18px] sm:text-[14px]">
                        <p className="text-sm font-bold text-navy-700 ">
                          {e.name}
                        </p>
                      </td>
                      <td className="pt-[14px] pb-[18px] sm:text-[14px]">
                        <p className="text-sm font-bold text-navy-700 ">
                          {e.role}
                        </p>
                      </td>
                      <td className="pt-[14px] pb-[18px] sm:text-[14px]">
                        {e.skills?.map((e, index) => {
                          return (
                            <>
                              <p
                                key={index}
                                className="text-sm my-2 font-bold text-navy-700 "
                              >
                                {e.skill[0]?.name}
                              </p>
                            </>
                          );
                        })}
                      </td>
                      <td className="pt-[14px] pb-[18px] sm:text-[14px]">
                        {e.skills?.map((e, index) => {
                          return (
                            <>
                              <p
                                key={index}
                                className="text-sm my-2 font-bold text-navy-700 "
                              >
                                {e.percentage}%
                              </p>
                            </>
                          );
                        })}
                      </td>
                      <td className="pt-[14px] pb-[18px] sm:text-[14px]">
                        <p
                          key={index}
                          className="text-sm my-2 font-bold text-navy-700 "
                        >
                          {(
                            (e.skills.reduce(
                              (val, cur) => val + cur.percentage,
                              0
                            ) /
                              (e.skills.length * 100)) *
                            100
                          ).toFixed(2) + "%"}
                          <div className="mt-3">
                            <Progress
                              width="w-[108px]"
                              color="indigo"
                              value={(
                                (e.skills.reduce(
                                  (val, cur) => val + cur.percentage,
                                  0
                                ) /
                                  (e.skills.length * 100)) *
                                100
                              ).toFixed(2)}
                            />
                          </div>
                        </p>
                      </td>
                      <td className="pt-[14px] pb-[18px] sm:text-[14px]">
                        <div className="flex">
                          <div
                            className="cursor-pointer"
                            onClick={() => {
                              setEdit(true);
                              setEditData(e);
                              setSkill(false);
                              setIsOpen(true);
                            }}
                          >
                            <svg
                              xmlns="http://www.w3.org/2000/svg"
                              x="0px"
                              y="0px"
                              width="18"
                              height="18"
                              viewBox="0 0 64 64"
                            >
                              <linearGradient
                                id="0O0q6J4HBgQKyT39nvTa~a_56304_gr1"
                                x1="46.807"
                                x2="46.807"
                                y1="9.849"
                                y2="24.215"
                                gradientUnits="userSpaceOnUse"
                                spreadMethod="reflect"
                              >
                                <stop offset="0" stop-color="#6dc7ff"></stop>
                                <stop offset="1" stop-color="#e6abff"></stop>
                              </linearGradient>
                              <path
                                fill="url(#0O0q6J4HBgQKyT39nvTa~a_56304_gr1)"
                                d="M49.482,24.392l-9.874-9.874l4.232-4.232c0.39-0.39,1.021-0.39,1.411,0l8.464,8.464 c0.39,0.39,0.39,1.021,0,1.411L49.482,24.392z"
                              ></path>
                              <linearGradient
                                id="0O0q6J4HBgQKyT39nvTa~b_56304_gr2"
                                x1="32"
                                x2="32"
                                y1="8.084"
                                y2="55.83"
                                gradientUnits="userSpaceOnUse"
                                spreadMethod="reflect"
                              >
                                <stop offset="0" stop-color="#1a6dff"></stop>
                                <stop offset="1" stop-color="#c822ff"></stop>
                              </linearGradient>
                              <path
                                fill="url(#0O0q6J4HBgQKyT39nvTa~b_56304_gr2)"
                                d="M50.697,25.999l4.428-4.428c1.167-1.167,1.167-3.065,0-4.232l-8.464-8.464 c-1.167-1.167-3.065-1.167-4.232,0l-4.428,4.428c-0.664-0.175-1.4-0.011-1.92,0.509l-1.411,1.411c-0.52,0.52-0.684,1.256-0.509,1.92 L11.198,40.106l-0.508,0.508l-0.2,0.2l-2.373,9.967c-0.343,1.442,0.078,2.928,1.126,3.976s2.534,1.469,3.976,1.125l9.967-2.373 l0.2-0.2l0.508-0.508l22.964-22.964c0.664,0.175,1.4,0.011,1.92-0.509l1.411-1.411C50.708,27.399,50.872,26.663,50.697,25.999z M47.367,27.92L36.081,16.634l1.411-1.411l11.285,11.285L47.367,27.92z M23.46,50.414c-0.28-1.063-0.682-2.077-1.198-3.034 l20.872-20.872l2.116,2.116L23.46,50.414z M14.916,53.428c-0.12-1.074-0.58-2.115-1.405-2.939c-0.825-0.825-1.865-1.285-2.939-1.405 l0.698-2.931c1.649,0.266,3.173,1.036,4.357,2.22c1.184,1.184,1.954,2.709,2.22,4.357L14.916,53.428z M17.038,46.962 c-1.447-1.447-3.301-2.396-5.306-2.75l0.463-1.943c2.382,0.441,4.533,1.562,6.254,3.282s2.841,3.872,3.282,6.254l-1.943,0.463 C19.433,50.263,18.485,48.409,17.038,46.962z M19.859,44.141c-0.477-0.477-0.987-0.907-1.517-1.304l20.561-20.561l2.821,2.821 L21.163,45.658C20.766,45.128,20.336,44.618,19.859,44.141z M16.62,41.738c-0.957-0.516-1.971-0.918-3.034-1.198l21.79-21.79 l2.116,2.116L16.62,41.738z M43.84,10.286c0.389-0.389,1.022-0.389,1.411,0l8.464,8.464c0.389,0.389,0.389,1.022,0,1.411 l-4.232,4.232l-9.874-9.874L43.84,10.286z"
                              ></path>
                            </svg>
                          </div>
                          <div
                            className="ml-2 cursor-pointer"
                            onClick={async () => {
                              await instance
                                .delete("/student?id=" + e._id)
                                .then(() => {
                                  alert("delete successfully");
                                  setREfresh(!isRefresh);
                                })
                                .catch(() => {
                                  alert("delete failed!");
                                });
                            }}
                          >
                            <svg
                              xmlns="http://www.w3.org/2000/svg"
                              x="0px"
                              y="0px"
                              width="18"
                              height="18"
                              viewBox="0 0 48 48"
                            >
                              <path
                                fill="red"
                                d="M 20.5 4 A 1.50015 1.50015 0 0 0 19.066406 6 L 14.640625 6 C 12.796625 6 11.086453 6.9162188 10.064453 8.4492188 L 7.6972656 12 L 7.5 12 A 1.50015 1.50015 0 1 0 7.5 15 L 40.5 15 A 1.50015 1.50015 0 1 0 40.5 12 L 40.302734 12 L 37.935547 8.4492188 C 36.913547 6.9162187 35.202375 6 33.359375 6 L 28.933594 6 A 1.50015 1.50015 0 0 0 27.5 4 L 20.5 4 z M 8.9726562 18 L 11.125 38.085938 C 11.425 40.887937 13.77575 43 16.59375 43 L 31.40625 43 C 34.22325 43 36.574 40.887938 36.875 38.085938 L 39.027344 18 L 8.9726562 18 z"
                              ></path>
                            </svg>
                          </div>
                        </div>
                      </td>
                    </tr>
                  </>
                );
              })}
            </tbody>
          </table>
        </div>
      </Card>
    </>
  );
};

export default DashBoard;

const PopUp = (props) => {
  const [is_error, setError] = useState([
    {
      skill: "",
      percentage: "",
    },
  ]);
  const [studentNameError, setStudentNameError] = useState("");
  const [studentRoleError, setStudentRoleError] = useState("");
  const [student_name, setStudentName] = useState(
    props.isEdit ? props.editData.name ?? "" : ""
  );
  const [role, setRole] = useState(
    props.isEdit ? props.editData.role ?? "" : ""
  );
  const [skills, setSkill] = useState(
    props.isEdit
      ? props.editData.skills.map((e) => {
          return {
            skill: e.skill[0]._id,
            percentage: e.percentage,
          };
        }) ?? [
          {
            skill: "",
            percentage: "",
          },
        ]
      : [
          {
            skill: "",
            percentage: "",
          },
        ]
  );

  return (
    <div>
      {/* student name */}
      <div className="flex flex-col my-3 ml-2">
        <label className="text-sm font-bold text-navy-700 ">Student Name</label>
        <input
          value={student_name}
          onChange={(e) => {
            setStudentName(e.target.value);
          }}
          placeholder="Enter Student Name"
          className="text-sm font-bold text-navy-700 border p-2"
        />
        <span className="text-[red] text-[12px]">
          {studentNameError && !student_name ? studentNameError : ""}
        </span>
      </div>
      <div className="flex flex-col my-3 ml-2">
        <label className="text-sm font-bold text-navy-700 ">Student Role</label>
        <input
          value={role}
          onChange={(e) => {
            setRole(e.target.value);
          }}
          placeholder="Enter Student Role"
          className="text-sm font-bold text-navy-700 border p-2"
        />
        <span className="text-[red] text-[12px]">
          {studentRoleError && !role ? studentRoleError : ""}
        </span>
      </div>
      {/* Enter Skill */}
      {skills.map((e, index) => {
        return (
          <div key={index}>
            <div className="flex ">
              <div className="flex flex-col my-3 mx-2">
                <label className="text-sm font-bold text-navy-700 ">
                  Student Skill
                </label>
                <select
                  value={e.skill}
                  onChange={(e) => {
                    let object = { ...skills[index] };
                    delete object.skill;
                    object.skill = e.target.value;
                    const skill = [...skills];
                    skill.splice(index, 1, object);
                    setSkill(skill);
                  }}
                  placeholder="Enter Student Name"
                  className="text-sm font-bold bg-white w-[170px] h-[37px] text-navy-700 border p-2"
                >
                  <option value={""} disabled>
                    Select Skill
                  </option>
                  {props.skillData?.map((e, index) => {
                    return (
                      <option key={index} value={e._id}>
                        {e.name}
                      </option>
                    );
                  })}
                </select>
                <span className="text-[red] text-[12px]">
                  {is_error[index]?.skill && !skills[index].skill
                    ? is_error[index]?.skill
                    : ""}
                </span>
              </div>
              <div className="flex flex-col my-3 mx-2">
                <label className="text-sm font-bold text-navy-700 ">
                  Skill Percentage
                  <span className="text-grey-600 text-[10px]">
                    (enter only number & max value 100)
                  </span>
                </label>
                <input
                  value={e.percentage}
                  onChange={(e) => {
                    if (Number(e.target.value) < 101) {
                      let object = { ...skills[index] };
                      delete object.percentage;
                      object.percentage = e.target.value;
                      const skill = [...skills];
                      skill.splice(index, 1, object);
                      setSkill(skill);
                    }
                  }}
                  placeholder="Enter Student Name"
                  className="text-sm font-bold text-navy-700 border p-2"
                />
                <span className="text-[red] text-[12px]">
                  {is_error[index]?.percentage && !skills[index].percentage
                    ? is_error[index]?.percentage
                    : ""}
                </span>
              </div>
              <div className="flex items-center mt-[20px] justify-center">
                {index == skills.length - 1 ? (
                  <AddCircleIcon
                    onClick={() => {
                      setSkill([
                        ...skills,
                        {
                          skill: "",
                          percentage: "",
                        },
                      ]);
                      setError([
                        ...is_error,
                        {
                          skill: "",
                          percentage: "",
                        },
                      ]);
                    }}
                    className="cursor-pointer text-[blue]"
                  />
                ) : (
                  ""
                )}

                {index != 0 ? (
                  <div
                    className="ml-2 cursor-pointer"
                    onClick={() => {
                      const skill = [...skills];
                      skill.splice(index, 1);
                      setSkill(skill);
                    }}
                  >
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      x="0px"
                      y="0px"
                      width="20"
                      height="20"
                      viewBox="0 0 48 48"
                    >
                      <path
                        fill="red"
                        d="M 20.5 4 A 1.50015 1.50015 0 0 0 19.066406 6 L 14.640625 6 C 12.796625 6 11.086453 6.9162188 10.064453 8.4492188 L 7.6972656 12 L 7.5 12 A 1.50015 1.50015 0 1 0 7.5 15 L 40.5 15 A 1.50015 1.50015 0 1 0 40.5 12 L 40.302734 12 L 37.935547 8.4492188 C 36.913547 6.9162187 35.202375 6 33.359375 6 L 28.933594 6 A 1.50015 1.50015 0 0 0 27.5 4 L 20.5 4 z M 8.9726562 18 L 11.125 38.085938 C 11.425 40.887937 13.77575 43 16.59375 43 L 31.40625 43 C 34.22325 43 36.574 40.887938 36.875 38.085938 L 39.027344 18 L 8.9726562 18 z"
                      ></path>
                    </svg>
                  </div>
                ) : (
                  ""
                )}
              </div>
            </div>
          </div>
        );
      })}
      <div className="flex my-3 justify-center mt-9 items-center">
        <div
          onClick={async () => {
            let skillCheck = true;
            const err = [];
            skills.forEach((e) => {
              let object = {};
              object.skill = e.skill ? "" : "skill is required";
              object.percentage = e.percentage ? "" : "percentage is required";
              if (!e.skill || !e.percentage) {
                err.push(object);
                skillCheck = false;
              } else {
                err.push({
                  skill: "",
                  percentage: "",
                });
              }
            });
            console.log(err);
            if (student_name && role && skillCheck) {
              if (props.isEdit) {
                await props.instance
                  .put("/student", {
                    id: props.editData?._id,
                    name: student_name,
                    role,
                    skills,
                  })
                  .then(() => {
                    props.closeOpen(false);
                    alert("student update successfully");
                  })
                  .catch((err) => {
                    alert("student update failed");
                  });
              } else {
                await props.instance
                  .post("/student", {
                    name: student_name,
                    role,
                    skills,
                  })
                  .then(() => {
                    props.closeOpen(false);
                    alert("student created successfully");
                  })
                  .catch((err) => {
                    alert("student created failed");
                  });
              }
            } else {
              setStudentNameError("student name is required");
              setStudentRoleError("student role is required");
              setError(err);
            }
          }}
          className="bg-[blue] cursor-pointer px-4 py-2 text-white rounded-[10px]"
        >
          Save
        </div>
      </div>
    </div>
  );
};

// add skills
const SkillPopUp = (props) => {
  const [name, setName] = useState("");
  const [onBlur, setOnBlur] = useState(false);
  const [is_error, set_isError] = useState("");

  return (
    <div>
      {/* student name */}
      <div className="flex flex-col my-3 ml-2">
        <label className="text-sm font-bold text-navy-700 ">Skill Name</label>
        <input
          value={name}
          onChange={(e) => {
            setName(e.target.value);
          }}
          onBlur={() => {
            setOnBlur(true);
          }}
          placeholder="Enter skill"
          className="text-sm font-bold text-navy-700 border p-2"
        />
        <span className="text-[red] text-[12px]">
          {is_error && onBlur && !name ? is_error : ""}
        </span>
      </div>

      <div className="flex my-3 justify-center mt-9 items-center">
        <div
          onClick={async () => {
            if (name) {
              await props.instance
                .post("/skill", {
                  name,
                })
                .then(() => {
                  props.closeOpen(false);
                  alert("skill created successfully");
                })
                .catch((err) => {
                  alert("skill created failed");
                });
            } else {
              set_isError("skill is required");
              setOnBlur(true);
            }
          }}
          className="bg-[blue] cursor-pointer px-4 py-2 text-white rounded-[10px]"
        >
          Save
        </div>
      </div>
    </div>
  );
};
