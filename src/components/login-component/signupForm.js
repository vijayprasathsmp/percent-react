import React, { useContext } from "react";
import {
  BoldLink,
  BoxContainer,
  FormContainer,
  Input,
  LineText,
  MutedLink,
  SubmitButton,
} from "./common";
import { useState } from "react";
import { Marginer } from "./marginer";
import { AccountContext } from "./accountContext";
import axios from "axios";

export function SignupForm(props) {
  const { switchToSignin } = useContext(AccountContext);
  const [userName, setUserName] = useState("");
  const [password, setPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");
  const [userNameError, setUserNameError] = useState("");
  const [passwordError, setPasswordError] = useState("");
  const [confirmPasswordError, setConfirmPasswordError] = useState("");
  const [is_req, setIsReq] = useState(false);
  return (
    <BoxContainer>
      <FormContainer>
        <Input
          type="text"
          value={userName}
          onChange={(e) => {
            setUserName(e.target.value);
          }}
          placeholder="user name"
        />
        <span className="text-[10px] text-[red]">
          {!userName && userNameError ? userNameError : ""}
        </span>
        <Input
          value={password}
          onChange={(e) => {
            setPassword(e.target.value);
          }}
          type="password"
          placeholder="Password"
        />
        <span className="text-[10px] text-[red]">
          {!password && passwordError ? passwordError : ""}
        </span>
        <Input
          value={confirmPassword}
          onChange={(e) => {
            setConfirmPassword(e.target.value);
            if (e.target.value != password) {
              setIsReq(false);
              setConfirmPasswordError("confirm password must match");
            } else {
              setConfirmPasswordError("");
            }
          }}
          type="password"
          placeholder="Confirm password"
        />
        <span className="text-[10px] text-[red]">
          {!confirmPassword
            ? is_req
              ? "confirm password required"
              : ""
            : confirmPasswordError
            ? confirmPasswordError
            : ""}
        </span>
      </FormContainer>
      <Marginer direction="vertical" margin={10} />
      <SubmitButton
        onClick={() => {
          if (confirmPassword == password && password && userName) {
            axios
              .post("http://localhost:3002/api/user", {
                user_name: userName,
                password: password,
              })
              .then((data) => {
                if (data.data?.status) {
                  window.location.reload();
                } else {
                  alert(data.data?.message);
                }
              })
              .catch((err) => {
                alert("user create failed");
              });
          } else {
            setUserNameError("user name is required");
            setPasswordError("password is required");
            setConfirmPasswordError("confirm password is required");
            setIsReq(true);
          }
        }}
        type="submit"
      >
        Signup
      </SubmitButton>
      <Marginer direction="vertical" margin="5px" />
      <LineText>
        Already have an account?{" "}
        <BoldLink onClick={switchToSignin} href="#">
          Signin
        </BoldLink>
      </LineText>
    </BoxContainer>
  );
}
