import React, { useContext, useState } from "react";
import {
  BoldLink,
  BoxContainer,
  FormContainer,
  Input,
  LineText,
  MutedLink,
  SubmitButton,
} from "./common";
import { Marginer } from "./marginer";
import { AccountContext } from "./accountContext";
import axios from "axios";
import { useNavigate } from "react-router-dom";

export function LoginForm(props) {
  const { switchToSignup } = useContext(AccountContext);
  const navigate = useNavigate();
  const [userName, setUserName] = useState("");
  const [password, setPassword] = useState("");

  return (
    <BoxContainer>
      <FormContainer>
        <Input
          type="text"
          onChange={(e) => {
            setUserName(e.target.value);
          }}
          placeholder="user name"
        />
        <Input
          onChange={(e) => {
            setPassword(e.target.value);
          }}
          type="password"
          placeholder="Password"
        />
      </FormContainer>
      <Marginer direction="vertical" margin={10} />
      <Marginer direction="vertical" margin="1.6em" />
      <SubmitButton
        onClick={() => {
          axios
            .post("http://localhost:3002/api/user/login", {
              user_name: userName,
              password: password,
            })
            .then((data) => {
              if (data.data?.status) {
                localStorage.setItem("token", data.data.token);
                navigate("/");
              } else {
                alert(data.data?.msg);
              }
            });
        }}
        type="submit"
      >
        Signin
      </SubmitButton>
      <Marginer direction="vertical" margin="5px" />
      <LineText>
        Don't have an accoun?{" "}
        <BoldLink onClick={switchToSignup} href="#">
          Signup
        </BoldLink>
      </LineText>
    </BoxContainer>
  );
}
