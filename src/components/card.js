function Card(props) {
  const { extra, children } = props;
  return (
    <div
      className={`!z-5 relative my-[10px] flex flex-col rounded-[20px] bg-white bg-clip-border shadow-3xl shadow-shadow-500 dark:!bg-navy-800  ${extra}`}
    >
      {children}
    </div>
  );
}

export default Card;
