import React, { useState } from "react";
import LoginPage from "./components/login-component/index";
import styled from "styled-components";
import DashBoard from "./components/pages/dashboard/dashboard";
import { Routes, Route, Navigate } from "react-router-dom";

const AppContainer = styled.div`
  width: 100%;
  height: 100vh;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`;

function App() {
  return (
    <>
      <div className="m-7">
        <Routes>
          <Route
            path="/login"
            element={
              <AppContainer>
                <LoginPage />
              </AppContainer>
            }
          />
          <Route path="/" element={<DashBoard />} />
        </Routes>
      </div>
    </>
  );
}

export default App;
